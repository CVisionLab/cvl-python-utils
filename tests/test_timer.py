"""Timer tests."""
import time

from cvl.utils import timer


def test_global_timer():
    """Test global timer."""
    time.sleep(0.01)
    assert (timer.stop('test_global_timer', return_only=True) > 0)


def test_local_less_global():
    """Test global timer started before local."""
    timer.start('test_local_less_global')
    t = timer.stop('test_local_less_global', return_only=True)
    assert(t < timer.stop('global'))


def test_non_existent_stats():
    """Test stats for non-existent timer are empty."""
    assert(len(timer.stats('test_non_existant_stats', return_only=True)) == 0)


def test_stats():
    """Test stats count."""
    for i in range(10):
        timer.start('test_stats')
        timer.stop('test_stats', return_only=True)
    stats = timer.stats('test_stats', return_only=True)
    assert(len(stats) == 10)


def test_time_delay():
    """Test time measurements."""
    for i in range(10):
        timer.start('test_stats')
        time.sleep(0.1)
        t = timer.stop('test_stats', return_only=True)
        assert(abs(t - 0.1) < 0.05)


def test_all_stats():
    """Test all stats exists and contain timers."""
    timer.start('test_all_stats')
    timer.stop('test_all_stats', return_only=True)

    all_stats = timer.stats(timer.ALL_STATS, return_only=True)
    assert(isinstance(all_stats, dict))
    assert('test_all_stats' in all_stats)


def test_elapsed():
    """Test elapsed time less than stop time."""
    timer.start('test_elapsed')
    time.sleep(0.1)
    elapsed = timer.elapsed('test_elapsed', return_only=True)
    time.sleep(0.1)
    t = timer.stop('test_elapsed', return_only=True)
    assert(t > elapsed)


def test_timer_stop_output(capsys):
    """Test output with stop()."""
    timer.start('test_timer_stop_output')
    timer.stop('test_timer_stop_output')
    out, _ = capsys.readouterr()
    assert(out == 'test_timer_stop_output: 0:00:00\n')
    timer.start('test_timer_stop_output')
    timer.stop('test_timer_stop_output')
    out, _ = capsys.readouterr()
    assert(out == 'test_timer_stop_output: 0:00:00 (mean 0:00:00 over 2)\n')


def test_timer_stats_output(capsys):
    """Test output with stop()."""
    timer.start('test_timer_stats_output')
    timer.stop('test_timer_stats_output')
    out, _ = capsys.readouterr()
    timer.stats('test_timer_stats_output')
    out, _ = capsys.readouterr()
    assert(out == 'test_timer_stats_output: mean 0:00:00 over 1\n')


def test_timer_elapsed_output(capsys):
    """Test output with stop()."""
    timer.start('test_timer_elapsed_output')
    timer.elapsed('test_timer_elapsed_output')
    out, _ = capsys.readouterr()
    assert(out == 'test_timer_elapsed_output: 0:00:00\n')

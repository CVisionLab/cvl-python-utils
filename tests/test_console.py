"""
Console functions tests.
"""
import re
import time
from cvl.utils import console


def test_progress(capsys):
    """
    Test progress display.
    """
    console.progress(0, 1, force=True)
    out, _ = capsys.readouterr()
    assert(re.search(r'\[-+\] 0.0%', out, re.MULTILINE) is not None)


def test_progress_notty(capsys):
    """
    Test progress doesn't display anythin in non tty env (like pytest).
    """
    console.progress(0, 1)
    out, _ = capsys.readouterr()
    assert(out == '')


def test_progress_fast_update(capsys):
    """
    Test repeated call doesn't result in output.
    """
    console.progress(0, 10, force=True)
    out, _ = capsys.readouterr()
    assert(re.search(r'\[-+\] 0.0%', out, re.MULTILINE) is not None)
    console.progress(1, 10, force=True)
    out, _ = capsys.readouterr()
    assert(out == '')


def test_progress_info_line(capsys):
    """
    Test progress second line.
    """
    console.progress(0, 10, text='infoline', force=True)
    out, _ = capsys.readouterr()
    assert(re.search(r'\[-+\] 0.0%', out, re.MULTILINE) is not None)
    assert(re.search(r'infoline', out, re.MULTILINE) is not None)


def test_progress_eta(capsys):
    """
    Test progress second line.
    """
    for i in range(7):
        console.progress(i, 10, eta=True, force=True)
        out, _ = capsys.readouterr()
        time.sleep(1)
    assert(re.search(r'\[ETA: 0:00:04[0-9.]*\]',
                     out, re.MULTILINE) is not None)

    console.progress(10, 10, eta=True, force=True)
    out, _ = capsys.readouterr()
    assert(re.search(r'\[Done in: 0:00:07[0-9.]*\]',
                     out, re.MULTILINE) is not None)


def test_term_control(capsys):
    """
    Test terminal control sequences.
    """
    console.control(console.ERASE_LINE, flush=True, force=True)
    out, _ = capsys.readouterr()
    assert(out == console.ERASE_LINE)

    console.control([console.ERASE_LINE, console.CURSOR_UP_ONE],
                    flush=True, force=True)
    out, _ = capsys.readouterr()
    assert(out == console.ERASE_LINE + console.CURSOR_UP_ONE)

"""Test sighandler."""
import os
import signal
import time
import pytest

from cvl.utils.console import Sighandler


def test_sighandler_sigint():
    """
    Test sighndler's ability to catch sigint.
    """
    handler = Sighandler()

    assert(handler.interrupted is False)

    os.kill(os.getpid(), signal.SIGINT)

    assert(handler.interrupted is True)

    handler.reset()
    assert(handler.interrupted is False)


def test_sighandler_force_exit():
    """
    Test sighndler's ability to perform force exit.
    """

    with pytest.raises(SystemExit):
        handler = Sighandler(terminate_sequence_timeout=1)

        assert(handler.interrupted is False)

        os.kill(os.getpid(), signal.SIGINT)
        os.kill(os.getpid(), signal.SIGINT)

        assert(handler.interrupted is True)


def test_sighandler_force_exit_timeout():
    """
    Test sighndler force exit timeout.
    """

    handler = Sighandler(terminate_sequence_timeout=1)

    assert(handler.interrupted is False)

    os.kill(os.getpid(), signal.SIGINT)
    time.sleep(2)
    os.kill(os.getpid(), signal.SIGINT)

    assert(handler.interrupted is True)


def test_sighandler_force_exit_disabled_with_0_timeout():
    """
    Test sighandler force exit disabled with 0 timeout.
    """
    handler = Sighandler(terminate_sequence_timeout=0)

    assert(handler.interrupted is False)

    os.kill(os.getpid(), signal.SIGINT)
    os.kill(os.getpid(), signal.SIGINT)

    assert(handler.interrupted is True)

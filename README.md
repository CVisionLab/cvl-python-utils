# CVL Python Utils

This repo contains simple utilities for python.

Package works correctly on python >= 3.6.

Full documentation can be found here: https://cvisionlab.gitlab.io/cvl-python-utils/ 

## Installation
Use `pip install git+https://gitlab.com/CVisionLab/cvl-python-utils.git` to install latest version or `pip install git+https://gitlab.com/CVisionLab/cvl-python-utils.git@<version>` to install specific version.

## Compatibility policy (supported python versions)
This repository follows [official status of python branches](https://devguide.python.org/#status-of-python-branches).

This means:
- We aim to add support for new versions as they appear in the list.
- We drop support for outdated versions when they are removed from the list.


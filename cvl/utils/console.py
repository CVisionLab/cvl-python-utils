"""
Console utilities.
"""

import sys as _sys
import collections as _collections
import signal as _signal
import os as _os


from . import timer as _timer

# console control sequences
CURSOR_UP_ONE = '\x1b[1A'
ERASE_LINE = '\x1b[2K'
CURSOR_TO_LINESTART = '\r'

_progress = {'last': 1, 'updated': 0}


def progress(count, total, label='', eta=False, text=None, force=False):
    """
    Display/update progress bar.

    Parameters
    ----------
    count : float
        Current progress value
    total : float
        Final progress value
    label : str, optional
        Text shown on the progress bar line
    eta : bool, optional
        If True display ETA time (on the next line)
    text : str, optional
        Text for the second (ETA) line
    force : bool, optional
        Force progress in case of running without tty
    """

    if not force and not _sys.stdout.isatty():
        return

    # get elapse from global init point
    ctime = _timer.elapsed(None, True)
    if ctime - _progress['updated'] < 0.1 and count != 0 and count != total:
        return
    _progress['updated'] = ctime

    bar_len = 60
    current_progress = count / float(total)
    filled_len = int(round(bar_len * current_progress))

    percents = round(100.0 * current_progress, 1)
    bar = '=' * filled_len + '-' * (bar_len - filled_len)

    _sys.stdout.write(ERASE_LINE)
    _sys.stdout.write('[{0}] {1}% {2}\r'.format(bar, percents, label))
    has_second_line = eta or text is not None
    if has_second_line:
        _sys.stdout.write('\n' + ERASE_LINE)
        if eta:
            if _progress['last'] > current_progress or current_progress == 0:
                _timer.start('_cvl.utils.progress', True)
            else:
                elapsed = _timer.elapsed('_cvl.utils.progress', True)
                if current_progress < 1:
                    if elapsed > 5:
                        eta = elapsed / current_progress - elapsed
                        _sys.stdout.write(
                            '[ETA: {0}] '.format(_timer.format_time(eta, 2))
                        )
                else:
                    _sys.stdout.write(
                        '[Done in: {0}] '.format(
                            _timer.format_time(elapsed, 2))
                    )
            _progress['last'] = current_progress
        _sys.stdout.write('{0}'.format(text or ''))
        if current_progress < 1:
            _sys.stdout.write('\r' + CURSOR_UP_ONE)
        else:
            _sys.stdout.write('\n')
    _sys.stdout.flush()


def control(code, flush=True, force=False):
    """
    Perform console control action.

    This function writes code to stdout and optionally flushes output.

    Parameters
    ----------
    code : str, iterable
        Console control code or iterable of console control codes
    flush : bool, optional
        if function should flush output
    force : bool, optional
        Force output of control codes in non tty environment.

    """

    if not force and not _sys.stdout.isatty():
        return

    if isinstance(code, str):
        _sys.stdout.write(code)
    elif isinstance(code, _collections.Iterable):
        codes = code
        for code in codes:
            _sys.stdout.write(code)
    if flush:
        _sys.stdout.flush()


def flush():
    """
    Flush stdout.
    """
    _sys.stdout.flush()


class Sighandler(object):
    """Handle interrupt signals."""

    def __init__(self, terminate_sequence_timeout=5):
        """
        Sighandler constructor.

        Parameters
        ----------
        terminate_sequence_timeout : int, optional
            Pressing Ctrl+C twice within this
            time forces immediate termination. Immediate termination disabled
            if terminate_sequence_timeout is 0.
        """
        super(Sighandler, self).__init__()
        self._terminate_sequence_timeout = terminate_sequence_timeout
        self._interrupted = False
        self._last_interruption_time = 0

        def sighandler(signal, frame):
            """Signal handler wrapper for the class method."""
            self._sighandler()
        _signal.signal(_signal.SIGINT, sighandler)

    @property
    def interrupted(self):
        """
        Return interrupted state (True if interruption was requested).
        """
        return self._interrupted

    def reset(self):
        """
        Reset interrupted state to False.
        """
        self._interrupted = False

    def _sighandler(self):
        """
        Actual signal handler.
        """
        # check for immediate termination
        if (self._terminate_sequence_timeout > 0) and (
                (_os.times()[4] - self._last_interruption_time) <=
                self._terminate_sequence_timeout):
            print('\nExiting per user request\n')
            _sys.exit(1)
        self._last_interruption_time = _os.times()[4]
        control([ERASE_LINE, CURSOR_TO_LINESTART], False)
        if self._interrupted:
            print('\nInterruption request received. Please wait...\n')
            flush()
            return
        self._interrupted = True
        if self._terminate_sequence_timeout > 0:
            print(('\nInterruption request received. Press Ctrl+C within {0} '
                   'second(s) to interrupt immediately\n').format(
                self._terminate_sequence_timeout))
        else:
            print('\nInterruption request received.\n')
        flush()

"""
Measures code execution time and calculates statistics.

Attributes
----------
ALL_STATS : None
    Constant to query stats for all timers.
"""
import os as os_
import datetime as _datetime
import math as _math
import copy

_timers = {}
_stats = {}
_globalstart = os_.times()[4]

ALL_STATS = None


def format_time(time_in_seconds, precision=-1):
    """
    Format time passed in seconds (with fraction) as a string.

    Parameters
    ----------
    time_in_seconds : float
        Time in seconds
    precision : int, optional
        Output precision (# of digits in fractional
        part of seconds), -1 means all digits

    Returns
    -------
    str
        Time as a string
    """
    if precision < 0:
        return str(_datetime.timedelta(seconds=time_in_seconds))
    fraction, integer = _math.modf(time_in_seconds)
    return (str(_datetime.timedelta(seconds=integer)) +
            '{0:.{1}f}'.format(fraction, precision)[1:])


def start(name, statsreset=False):
    """
    Start timer with specified name.

    Parameters
    ----------
    name : str
        Timer name.
    statsreset : bool, optional
        Reset timing statistics for this timer flag.
    """
    _timers[name] = os_.times()[4]
    if (name not in _stats) or statsreset:
        _stats[name] = []


def stop(name, return_only=False, show_stats=True):
    """
    Stop timer with specified name and display (or return) passed time.

    If timer with specified name was not started thet displays time from some
    global initialization point.

    If timer was started several times the simple statistics will be displayed:
    average time and number of samples.

    Parameters
    ----------
    name : str
        Timer name.
    return_only : bool, optional
        Flag to return passed time instead of printing.
    show_stats : bool, optional
        Description

    Returns
    -------
    None | float
        None or passed time depending on return_only argument.
    """
    if name not in _timers:
        time = os_.times()[4] - _globalstart
        if return_only:
            return time
        print('[G] {0}: {1}'.format(name, format_time(time)))
        return time
    time = os_.times()[4] - _timers[name]
    _stats[name].append(time)
    if return_only:
        return time
    if len(_stats[name]) > 1 and show_stats:
        print('{0}: {1} (mean {2} over {3})'.format(
            name, format_time(time),
            format_time(sum(_stats[name]) / len(_stats[name])),
            len(_stats[name])))
    else:
        print('{0}: {1}'.format(name, format_time(time)))
    return time


def stats(name, return_only=False):
    """
    Print statistics for timer with specified name.

    Pass timer.ALL_STATS as name to print all statistics.

    Parameters
    ----------
    name : str
        Timer name or timings.ALL_STATS to print all statistics.
    return_only : bool, optional
        Flag to return stats instead of printing.

    Returns
    -------
    None | list | dict
        Returns None if return_only is False. Return list if supplied with a
        name of timer. Returns dict with all timers stats if timer.ALL_STATS
        constant passed.
    """
    if return_only is True:
        if name != ALL_STATS:
            return _stats.get(name, [])
        else:
            return copy.deepcopy(_stats)

    names = [name] if name != ALL_STATS else sorted(_stats.keys())
    for name in names:
        print('{0}: mean {1} over {2}'.format(
            name, format_time(sum(_stats[name]) / len(_stats[name])),
            len(_stats[name]))
        )


def elapsed(name, return_only=False):
    """
    Display (or return) elapsed time for a timer with specified name.

    If timer with specified name was not started then displays time from some
    global initialization point.

    Parameters
    ----------
    name : str
        Timer name.
    return_only : bool, optional
        Flag to return passed time instead of printing

    Returns
    -------
    None | float
        None or passed time depending on return argument.
    """
    time = os_.times()[4] - _timers.get(name, _globalstart)
    if return_only:
        return time
    print('{0}: {1}'.format(name, format_time(time)))

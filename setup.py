"""Small utilities: console and timer"""

from setuptools import setup, find_namespace_packages

setup(
    name='cvl-utils',
    use_scm_version=True,
    setup_requires=['setuptools_scm'],
    description=__doc__,
    author='Dmitri Lapin',
    author_email='lapin@cvisionlab.com',
    packages=find_namespace_packages(include=['cvl.*']),
    python_requires='>=3.6',
    classifiers=[
        'Programming Language :: Python :: 3',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
    ],
    zip_safe=False
)

get_refs() {
    get_refs_usage() {
        cat <<EOM
Usage: $0 [-t <tag|branch|all>] [-s <search>] [-n <number>] [-c <commit-sha>] [-i] [-h]

Print tags and / or branches of current commit.

This script relies on \$PRIVATE_CI_TOKEN environment variable to perform API
calls to Gitlab.

    -t      Type of refs to show (default is all).

    -s      Only print refs that match regular expression (egrep).
            Regular expression must not match double quote.
            Use '[^"]*' to match anything instead of '.*'.

    -n      Number of refs to print (default is to print all).

    -c      Use passed commit instead of the current.

    -i      Include current ref (\$CI_COMMIT_REF_NAME).
            Default is to omit this ref.

    -h      Display this help message and exit.
EOM
    1>&2; return 1;
    }

    # Set defaults
    TYPE=all
    SEARCH='[^"]*'
    COMMIT=$CI_COMMIT_SHA

    # Parse command line
    local OPTIND o
    while getopts ":t:s:n:c:ih" o; do
        case "${o}" in
            t)
                TYPE=${OPTARG}
                if [ "$TYPE" != "all" ] && [ "$TYPE" != "tag" ] && [ "$TYPE" != "branch" ]; then
                    get_refs_usage
                    return
                fi
                ;;
            s)
                SEARCH=${OPTARG}
                ;;
            n)
                LIMIT=${OPTARG}
                ;;
            c)
                COMMIT=${OPTARG}
                ;;
            i)
                INCLUDE=yes
                ;;
            h|*)
                get_refs_usage
                return
                ;;
        esac
    done
    shift $((OPTIND-1))

    # Get all refs
    REFS_LIST=$(wget -q --header "PRIVATE-TOKEN: $PRIVATE_CI_TOKEN" "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/repository/commits/$COMMIT/refs?type=$TYPE" -O-)

    # Filter refs. This should be before sed to make different entries appear on
    # separate lines.
    REFS_LIST=$(echo "$REFS_LIST" | grep -oe "{\"type\":\s*\"[^\"]*\",\s*\"name\":\s*\"$SEARCH\"}")

    # Leave only ref names.
    REFS_LIST=$(echo "$REFS_LIST" | sed -r 's/\{"type":\s*"[^"]*",\s*"name":\s*"([^"]*)"\}/\1/')

    # If not asked to keep current ref -- remove it
    if [ "$INCLUDE" != "yes" ]; then
        REFS_LIST=$(echo "$REFS_LIST" | grep -v "$CI_COMMIT_REF_NAME")
    fi

    # Limit number of refs (if asked to do so).
    if [ ! -z "$LIMIT" ]; then
        REFS_LIST=$(echo "$REFS_LIST" | head -n $LIMIT)
    fi

    echo "$REFS_LIST"
}

alias get-refs='get_refs'

get_pipelines() {
    get_pipelines_usage() {
        cat <<EOM
Usage: $0 [-r <ref-match>] [-p <project-id>] [-t <private-token>] [-s <tags|branches>] [-n <ref-name>] [-l <limit>] [-h]

Output JSON objects describing pipelines optionally filtered by ref name,
pipeline name and scope.

This script relies on existence of "jq" executable to be either on PATH
or set via "JQ" environment variable. The jq doesn't require installation and
can be downloaded here: https://stedolan.github.io/jq/.

    -r      Regular expression to match ref names against.
            Default is to not filter by ref.

    -p      Project identifier (number) or project slug (including namespace).
            Default is to use value of CI_PROJECT_ID environment variable.

    -t      Private API-token.
            Default is to use value of PRIVATE_CI_TOKEN environment variable.

    -s      Scope to search pipeline:
                * tags - Pipelines triggered as a result of tag push events.
                * branches - Pipelines triggered as a result of branch push
                  events.
            The other scopes available in pipelines API make no sense for
            successful pipelines.

    -n      Exact ref name (if known). The -r parameters gives more abilities,
            but this parameter can be more efficient as it is directly supported
            by GitLab and filtering runs at serer side.

    -l      Limit number of returned pipelines.

    -h      Display this help message and exit.
EOM
    1>&2; return 1;
    }

    # Set defaults
    local JQ="${JQ:-jq}"
    local PRIVATE_TOKEN=$PRIVATE_CI_TOKEN
    local PROJECT_ID=${CI_PROJECT_ID//'/'/%2F}
    local REF_REGEXP="."
    local NL="
"
    local JSON_OUTPUT
    local SCOPE
    local REF_NAME
    local LIMIT=0

    # Parse command line
    local OPTIND o
    while getopts ":r:p:s:t:n:l:h" o; do
        case "${o}" in
            r)
                REF_REGEXP=${OPTARG}
                ;;
            p)
                PROJECT_ID=${OPTARG//'/'/%2F}
                ;;
            s)
                SCOPE=${OPTARG}
                if [ "$SCOPE" != "tags" ] && [ "$SCOPE" != "branches" ]; then
                    get_pipelines_usage
                    return
                fi
                ;;
            t)
                PRIVATE_TOKEN=${OPTARG}
                ;;
            n)
                REF_NAME=${OPTARG}
                ;;
            l)
                LIMIT=${OPTARG}
                ;;
            j)
                JSON_OUTPUT=1
                ;;
            h|*)
                get_pipelines_usage
                return
                ;;
        esac
    done
    shift $((OPTIND-1))

    local ENDPOINT="https://gitlab.com/api/v4/projects/$PROJECT_ID/pipelines?status=success&sort=desc&order_by=id"

    if [ ! -z "$SCOPE" ]; then
        ENDPOINT="$ENDPOINT&scope=$SCOPE"
    fi

    if [ ! -z "$REF_NAME" ]; then
        ENDPOINT="$ENDPOINT&scope=$REF_NAME"
    fi
    local RESULT=""
    while [ ! -z "$ENDPOINT" ]; do
        # Get next page link
        local NEXT_PAGE=$(wget -qS --spider --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" "$ENDPOINT" 2>&1 | grep "^\s\+Link:" | sed -nr 's/^.*<([^>]*)>; rel="next".*$/\1/p')

        # Get all refs
        REFS_LIST=$(wget -q --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" "$ENDPOINT" -O- | $JQ -c --arg project "$PROJECT_ID" '.[] + {project: $project}')

        # Filter
        REFS_LIST=$(echo "$REFS_LIST" | $JQ -c 'select(.ref|test("'$REF_REGEXP'"))')

        if [ ! -z "$REFS_LIST" ]; then
            if [ ! -z "$RESULT" ]; then
                RESULT="$RESULT$NL"
            fi
            RESULT="$RESULT$REFS_LIST"
        fi

        if [ ! -z "$RESULT" ] && [ "$LIMIT" -gt 0 ] && [ $(echo "$RESULT" | wc -l) -ge "$LIMIT" ]; then
            echo "$RESULT" | head -n "$LIMIT"
            return 0
        fi

        ENDPOINT=$NEXT_PAGE
    done
    echo "$RESULT"
}

alias get-pipelines='get_pipelines'

get_jobs() {
    get_jobs_usage() {
        cat <<EOM
Usage: $0 [-m <job-match>] [-t <private-token>] [-l <limit>] [-a] [-h] [<pipelines>]

Get last successful jobs of pipelines (JSON) read from file or stdin.

Each pipeline must a separate JSON object, one object per line. A pipeline must
have the following fields (other fields are ignored):
- "project"
- "id"

This script relies on existence of "jq" executable to be either on PATH
or set via "JQ" environment variable. The jq doesn't require installation and
can be downloaded here: https://stedolan.github.io/jq/.

    -m      Regular expression to match job names against.
            Default is to not filter by name.

    -t      Private API-token.
            Default is to use value of PRIVATE_CI_TOKEN environment variable.

    -l      Limit number of returned pipelines.

    -a      Return only jobs with artifacts.

    -h      Display this help message and exit.

    <pipelines>
            File with pipelines JSON, one per line. If not set pipelines are
            read from stdin.

EOM
    1>&2; return 1;
    }

    # Set defaults
    local JQ="${JQ:-jq}"
    local PRIVATE_TOKEN=$PRIVATE_CI_TOKEN
    local PROJECT_ID_PASSED=
    local NAME_REGEXP=
    local NL="
"
    local LIMIT=0
    local REQUIRE_ARTIFACTS

    # Parse command line
    local OPTIND o
    while getopts ":m:t:l:ah" o; do
        case "${o}" in
            m)
                NAME_REGEXP=${OPTARG}
                ;;
            t)
                PRIVATE_TOKEN=${OPTARG}
                ;;
            l)
                LIMIT=${OPTARG}
                ;;
            a)
                REQUIRE_ARTIFACTS=1
                ;;
            h|*)
                get_jobs_usage
                return
                ;;
        esac
    done
    shift $((OPTIND-1))

    local RESULT=""

    while read PIPELINE
    do
        if [ -z "$PIPELINE" ]; then
            continue
        fi

        local PROJECT_ID="$(echo "$PIPELINE" | $JQ -cr '.project')"
        local PIPELINE_ID="$(echo "$PIPELINE" | $JQ -cr '.id')"
        local ENDPOINT="https://gitlab.com/api/v4/projects/$PROJECT_ID/pipelines/$PIPELINE_ID/jobs?scope=success"

        while [ ! -z "$ENDPOINT" ]; do
            # Get next page link
            local NEXT_PAGE=$(wget -qS --spider --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" "$ENDPOINT" 2>&1 | grep "^\s\+Link:" | sed -nr 's/^.*<([^>]*)>; rel="next".*$/\1/p')

            # Get jobs
            JOB_LIST=$(wget -q --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" "$ENDPOINT" -O- | $JQ -c --arg project "$PROJECT_ID" '.[] + {project: $project}')

            # Filter by name
            if [ ! -z "$NAME_REGEXP" ]; then
                JOB_LIST=$(echo "$JOB_LIST" | $JQ -c 'select(.name|test("'$NAME_REGEXP'"))')
            fi

            # Filter by artifacts existence
            if [ ! -z "$REQUIRE_ARTIFACTS" ]; then
                JOB_LIST=$(echo "$JOB_LIST" | $JQ -c 'select(.artifacts_file)')
            fi


            if [ ! -z "$JOB_LIST" ]; then
                if [ ! -z "$RESULT" ]; then
                    RESULT="$RESULT$NL"
                fi
                RESULT="$RESULT$JOB_LIST"
            fi

            if [ ! -z "$RESULT" ] && [ "$LIMIT" -gt 0 ] && [ $(echo "$RESULT" | wc -l) -ge "$LIMIT" ]; then
                echo "$RESULT" | head -n "$LIMIT"
                return 0
            fi

            ENDPOINT=$NEXT_PAGE
        done
    done < "${1:-/dev/stdin}"

    echo "$RESULT"
}

alias get-jobs='get_jobs'


get_artifacts() {
    get_artifacts_usage() {
        cat <<EOM
Usage: $0 (-f <target-file> | -d <target-directory>) [-t <private-token>] [-e] [-p] [-r] [-h] [<jobs>]

Download job artifacts archive given the jobs JSON (from file or stdin).

Each job must a separate JSON object, one object per line. A job must
have the following fields (other fields are ignored):
- "project"
- "id"
- "artifacts_file.filename"

This script relies on existence of "jq" executable to be either on PATH
or set via "JQ" environment variable. The jq doesn't require installation and
can be downloaded here: https://stedolan.github.io/jq/.

    -f      Path to the file to save downloaded archive to.
            Can't be used together with -d option.

    -d      Path to the directory o save downloaded archive to.
            Artifacts archive saved with the original name obtained from Gitlab.
            Can't be used together with -f option.

    -t      Private API-token.
            Default is to use value of PRIVATE_CI_TOKEN environment variable.

    -r      Require at least one artifact to be downloaded.

    -p      Display progress while downloading.

    -h      Display this help message and exit.

    <jobs>
            File with jobs JSON, one per line. If not set jobs are read from
            stdin.

EOM
    1>&2; return 1;
    }

    # Set defaults
    local JQ="${JQ:-jq}"
    local PRIVATE_TOKEN=$PRIVATE_CI_TOKEN
    local SHOW_PROGRESS=
    local REQUIRE_ARTIFACTS
    local FILE_TARGET=
    local DIR_TARGET=

    # Parse command line
    local OPTIND o
    while getopts ":f:d:t:erph" o; do
        case "${o}" in
            f)
                FILE_TARGET=${OPTARG}
                ;;
            d)
                DIR_TARGET=${OPTARG}
                ;;
            t)
                PRIVATE_TOKEN=${OPTARG}
                ;;
            p)
                SHOW_PROGRESS="--show-progress --progress=dot:giga"
                ;;
            r)
                REQUIRE_ARTIFACTS=1
                ;;
            h|*)
                get_artifacts_usage
                return
                ;;
        esac
    done
    shift $((OPTIND-1))

    # At least on should be set
    if [ -z "$FILE_TARGET" ] && [ -z "$DIR_TARGET" ]; then
        get_artifacts_usage
        return
    fi

    # Shouldn't be set both
    if [ ! -z "$FILE_TARGET" ] && [ ! -z "$DIR_TARGET" ]; then
        get_artifacts_usage
        return
    fi

    local DOWNLOADED=0

    while read PIPELINE
    do
        if [ -z "$PIPELINE" ]; then
            continue
        fi
        local PROJECT_ID=$(echo "$PIPELINE" | $JQ -cr '.project')
        local JOB_ID="$(echo "$PIPELINE" | $JQ -cr '.id')"

        # Build file target
        if [ ! -z "$DIR_TARGET" ]; then
            FILE_TARGET="$DIR_TARGET/$(echo "$PIPELINE" | $JQ -cr '.artifacts_file.filename')"
        fi

        # make sure output directory exists
        mkdir -p "$(dirname "$FILE_TARGET")"

        wget -q $SHOW_PROGRESS --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" "https://gitlab.com/api/v4/projects/$PROJECT_ID/jobs/$JOB_ID/artifacts" -O$FILE_TARGET

        retVal=$?
        if [ $retVal -eq 0 ]; then
            DOWNLOADED=1
            echo "$FILE_TARGET"
        fi
    done < "${1:-/dev/stdin}"

    if [ $DOWNLOADED -eq 0 ] && [ ! -z "$REQUIRE_ARTIFACTS" ]; then
        (>&2 echo "No files were downloaded but required (-r).")
        exit 1
    fi
}

alias get-artifacts='get_artifacts'

publish_release() {
    publish_release_usage() {
        cat <<EOM
Usage: $0 [-t <private-token>] [-p <project-id-or-slug>] [-n <release-name>]
    [[-a <asset-name> <asset-url>], ...] [-h] [<description>]

Publish release notes with optional assets.

This script relies on existence of "jq" executable to be either on PATH
or set via "JQ" environment variable. The jq doesn't require installation and
can be downloaded here: https://stedolan.github.io/jq/.

    -t      Private API-token.
            Default is to use value of PRIVATE_CI_TOKEN environment variable.

    -p      Project identifier (number) or project slug (including namespace).
            Default is to use value of CI_PROJECT_ID environment variable.

    -n      Release name. Defaults to CI_COMMIT_TAG environment variable.

    -h      Display this help message and exit.

    -a <asset-name> <asset-url>
            Add asset with the provided name and URL. Can be specified multiple
            times.

    <release-description>
            Release notes.

EOM
    1>&2; return 1;
    }

    # Set defaults
    local JQ="${JQ:-jq}"
    local PRIVATE_TOKEN=$PRIVATE_CI_TOKEN
    local RELEASE_NAME=$CI_COMMIT_TAG
    local PROJECT_ID=${CI_PROJECT_ID//'/'/%2F}

    # Build initial object, without assets.
    local JSON_PAYLOAD=$($JQ -n --arg tag_name "$CI_COMMIT_TAG" '{tag_name: $tag_name, assets: {links: []}}')

    # Parse command line
    local OPTIND o
    while getopts ":t:n:p:a:h" o; do
        case "${o}" in
            t)
                PRIVATE_TOKEN=${OPTARG}
                ;;
            p)
                PROJECT_ID=${OPTARG//'/'/%2F}
                ;;
            a)
                ASSET_NAME=${OPTARG}
                ASSET_URL=${!OPTIND}
                # If URL starts from "-", then it's a next option
                if [[ "$ASSET_URL" =~ -.* ]]; then
                    publish_release_usage
                    return
                fi
                # Add assets to the array
                JSON_PAYLOAD=$(echo "$JSON_PAYLOAD" | $JQ --arg asset_name "$ASSET_NAME" --arg asset_url "$ASSET_URL" '.assets.links += [{name: $asset_name, url: $asset_url}]')
                # Shift after consuming additional value.
                OPTIND=$((OPTIND+1))
                ;;
            n)
                RELEASE_NAME=${OPTARG}
                ;;
            h|*)
                publish_release_usage
                return
                ;;
        esac
    done
    shift $((OPTIND-1))

    if [ -z "$1" ]; then
        publish_release_usage
        return
    fi

    # Add release name and description. Couldn't add earlier as options were not parsed.
    JSON_PAYLOAD=$(echo "$JSON_PAYLOAD" | $JQ --arg name "$RELEASE_NAME" --arg description "$1" '.name=$name | .description=$description')

    # Do API call.
    wget -q --header 'Content-Type: application/json' --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" \
     --post-data "$JSON_PAYLOAD" "https://gitlab.com/api/v4/projects/$PROJECT_ID/releases"
}

alias publish-release='publish_release'
